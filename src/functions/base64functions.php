<?php

function saveFileToServer($data) {
  $returnData = array();
  $name = $data['meta']['name'];
  // var_dump($data);
  // $fh = fopen("./tmp/$name", 'w') or die('cant');
  // $fh = fopen("../tmp/$name", 'w');
  $file = fopen("./tmp/$name", "wb");
  $base64_string = explode(',', $data['data']);
  fwrite($file, base64_decode($base64_string[1]));
  fclose($file);

  return $returnData;
}

function base64GetFileData($fileData) {
  $data = array("imageType" => null,
                "imageBase64" => null,
                "imageBase64Decoded" => null);
  $imageParts = explode(";base64,", $fileData);
  $imageTypeAux = explode("image/", $imageParts[0]);
  $data['imageType'] = $imageTypeAux[1];
  $data['imageBase64'] = $imageParts[1];
  $data['imageBase64Decoded'] = base64_decode($imageParts[1]);
  return $data;
}