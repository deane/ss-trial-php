<?php

	function createData($db, $sqlData, $ignore = false) {
		if (isset($sqlData['condition'])) {
			$sql = "INSERT " . ($ignore ? "IGNORE " : "") . "INTO
    `" . $sqlData['table'] . "` (" . $sqlData['fields'] . ")
    VALUES
    (" . $sqlData['bindings'] . ") " . $sqlData['condition'];
		} else {
			$sql = "INSERT INTO
    `" . $sqlData['table'] . "` (" . $sqlData['fields'] . ")
    VALUES
    (" . $sqlData['bindings'] . ")";
		}
		 
		$stmt = $db->prepare($sql);
		$stmt->execute($sqlData['params']);
		$id = $db->lastInsertId();
		return $id;
	}

	function readData($db, $sqlData) {
		if (strlen($sqlData['table']) < 15) {
			$sqlData['table'] = "`{$sqlData['table']}`";
		}
		$query = "SELECT " . $sqlData['fields'] . " FROM " . $sqlData['table'] . " " . $sqlData['condition'];
		// echo $query;
		$stmt = $db->prepare($query);
		if (isset($sqlData['params'])) {
			$stmt->execute($sqlData['params']);
		} else {
			$stmt->execute();
		}
		return $stmt;
	}

	function updateData($db, $sqlData) {
		$query = "UPDATE `" . $sqlData['table'] . "` SET " . $sqlData['fields'] . " " . $sqlData['condition'];
		$stmt = $db->prepare($query);
		$stmt->execute($sqlData['params']);
	}

	function updateDataNested($db, $sqlData) {
		$query = "INSERT INTO
  `" . $sqlData['table'] . "` (" . $sqlData['fields'] . ")
  VALUES
  (" . $sqlData['id'] . "" . $sqlData['bindings'] . ")
  ON DUPLICATE KEY UPDATE
  " . $sqlData['duplicateData'] . "";
		$stmt = $db->prepare($query);
		$stmt->execute($sqlData['params']);
	}

	function deleteSecure($db, $sqlData, $virtualDelete = false) {
		if (!$virtualDelete) {
			$query = "DELETE FROM
  `" . $sqlData['table'] . "` " . $sqlData['condition'];
			$stmt = $db->prepare($query);
			$stmt->execute($sqlData['params']);
		} else {
			$query = "UPDATE `" . $sqlData['table'] . "` SET deleted_ts = now() " . $sqlData['condition'];
			$stmt = $db->prepare($query);
			$stmt->execute($sqlData['params']);
		}
	}

	function getColumnDetails($db, $table) {
		$query = "SHOW COLUMNS FROM $table";
		$stmt = $db->prepare($query);
		$stmt->execute();
		return $stmt;
	}