<?php

// Chrome Debugger Functions

function consoleLog($data) {
  ChromePhp::log($data);
}

function consoleWarn($data) {
  ChromePhp::warn($data);
}

// General Functions

function convert_smart_quotes($string) {
  $search = array(
    chr(145),
    '’',
    '–',
    chr(146),
    chr(147),
    chr(148),
    chr(151),
  );

  $replace = array(
    "'",
    "'",
    '-',
    "'",
    '"',
    '"',
    '-',
  );

  return str_replace($search, $replace, $string);
}

function generateRandomString($length) {
  $bytes = random_bytes(5);
  return bin2hex($bytes);
}

function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds') {
  $sets = array();
  if (strpos($available_sets, 'l') !== false) {
    $sets[] = 'abcdefghjkmnpqrstuvwxyz';
  }

  if (strpos($available_sets, 'u') !== false) {
    $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
  }

  if (strpos($available_sets, 'd') !== false) {
    $sets[] = '23456789';
  }

  if (strpos($available_sets, 's') !== false) {
    $sets[] = '!@#$%&*?';
  }

  $all = '';
  $password = '';
  foreach ($sets as $set) {
    $password .= $set[array_rand(str_split($set))];
    $all .= $set;
  }

  $all = str_split($all);
  for ($i = 0; $i < $length - count($sets); $i++) {
    $password .= $all[array_rand($all)];
  }

  $password = str_shuffle($password);

  if (!$add_dashes) {
    return $password;
  }

  $dash_len = floor(sqrt($length));
  $dash_str = '';
  while (strlen($password) > $dash_len) {
    $dash_str .= substr($password, 0, $dash_len) . '-';
    $password = substr($password, $dash_len);
  }
  $dash_str .= $password;
  return $dash_str;
}

function cleanBodyData($data) {
  foreach ($data as $key => $value) {
    if ($value == "") {
      $data[$key] = NULL;
    }
  }
  return $data;
}

function matchBodyToTable($db, $data, $table) {
  $columns = loadSecureTableColumns($db, $table);
  $returnData = array();
  foreach ($data as $key => $value) {
    if (in_array($key, $columns)) {
      $returnData[$key] = $value;
    }
  }
  return $returnData;

}

// GENERA: SQL

function restrictFields($data, $restrictedFields) {
  foreach ($data as $key => $value) {
    if (in_array($key, $restrictedFields)) {
      unset($data[$key]);
    }
  }
  return $data;
}

function fieldsToString($array) {
  $return = 'id';
  foreach ($array as $key => $value) {
    $return = $return . ", `" . $value . "`";
  }
  return $return;
}

function fieldsToStringDefinedID($array) {
  $return = "";
  $first = true;
  foreach ($array as $key => $value) {
    if ($first) {
      $return = $return . $value;
      $first = false;
    } else {
      $return = $return . ", " . $value;
    }

  }
  return $return;
}

function getFieldsAndDataSingle($data) {
  $return = array("fields" => array(),
    "data" => array(),
    "bindings" => 'NULL',
  );
  foreach ($data as $key => $value) {
    if ($key != "id") {
      array_push($return['fields'], $key);
      if ($key == 'geoPoint') {
        array_push($return['data'], $value[0]);
        array_push($return['data'], $value[1]);
        $return['bindings'] = $return['bindings'] . ', POINT(?, ?)';
      } else {
        if ($value === '') {
          $value = NULL;
        }
        array_push($return['data'], $value);
        $return['bindings'] = $return['bindings'] . ', ?';
      }

    }
  }
  return $return;
}

function getFieldsAndDataSingleDefinedID($data) {
  $first = true;
  $return = array("fields" => array(),
    "data" => array(),
    "bindings" => '',
  );
  foreach ($data as $key => $value) {
    if ($key == 'geopoint') {
      array_push($return['data'], $value[0]);
      $return['bindings'] = $return['bindings'] . ', ?';
      $value = $value[1];
    }
    if ($value === '') {
      $value = NULL;
    }
    array_push($return['fields'], $key);
    array_push($return['data'], $value);
    if ($first) {
      $return['bindings'] = $return['bindings'] . '?';
      $first = false;
    } else {
      $return['bindings'] = $return['bindings'] . ', ?';
    }

  }
  return $return;
}

function getRelatedArray($data, $id, $relatedKey) {
  $returnArray = array(
    "notRelated" => array(),
    "related" => array(),
  );

  foreach ($data as $key => $value) {
    if ($value[$relatedKey] == $id) {
      array_push($returnArray['related'], $value);
      unset($data[$key]);
    }
  }
  $returnArray['notRelated'] = $data;

  return $returnArray;

}

// CREATE

function createSqlData($body, $table) {
  $fieldsAndData = getFieldsAndDataSingle($body);
  $fieldsString = fieldsToString($fieldsAndData['fields']); // turning fields array into a string
  $sqlData = array("fields" => $fieldsString,
    "table" => $table,
    "params" => $fieldsAndData['data'],
    "bindings" => $fieldsAndData['bindings']); //Creating an object with all of the data
  return $sqlData;
}

function createSecure($db, $sqlData, $ignore = false) {
  $id = createData($db, $sqlData, $ignore);
  $idKey = $sqlData['table'] . '_id';
  $return = array("Result" => "Success", $idKey => $id);
  return $return;
  // return "testing";
}

function createSqlDataDefinedID($body, $table) {
  $fieldsAndData = getFieldsAndDataSingleDefinedID($body);
  $fieldsString = fieldsToStringDefinedID($fieldsAndData['fields']); // turning fields array into a string
  $sqlData = array("fields" => $fieldsString,
    "table" => $table,
    "params" => $fieldsAndData['data'],
    "bindings" => $fieldsAndData['bindings']); //Creating an object with all of the data
  return $sqlData;
}

function createSecureDefinedID($db, $sqlData, $id) {
  createData($db, $sqlData);
  $idKey = $sqlData['table'] . '_id';
  $return = array("Result" => "Success", $idKey => $id);
  return $return;
}

// UPDATE

function updateFieldsToString($data) {
  $return = '';
  $first = true;
  foreach ($data as $key => $value) {
    if ($value == "id") {
      $return = $return;
    } else if ($first) {
      $return = "`$value` = ?";
      $first = false;
    } else if ($value == "geopoint") {
      $return = $return . ", $value = POINT(?, ?)";
    } else {
      $return = $return . ", `$value` = ?";
    }
  }
  return $return;
}

function updateSqlData($body, $table, $id) {
  $fieldsAndData = getFieldsAndDataSingle($body); // creating array with Fields, Data, and Bindings for sql
  $fieldsString = updateFieldsToString($fieldsAndData['fields']);
  array_push($fieldsAndData['data'], $id);
  $sqlData = array("fields" => $fieldsString,
    "table" => $table,
    "params" => $fieldsAndData['data'],
    "condition" => "WHERE id = ?"); //Creating an object with all of the data
  return $sqlData;
}

function updateSqlDataParams($body, $table, $condition, $params) {
  $fieldsAndData = getFieldsAndDataSingle($body); // creating array with Fields, Data, and Bindings for sql
  $fieldsString = updateFieldsToString($fieldsAndData['fields']);
  foreach ($params as $key => $value) {
    array_push($fieldsAndData['data'], $value);
  }
  $sqlData = array("fields" => $fieldsString,
    "table" => $table,
    "params" => $fieldsAndData['data'],
    "condition" => $condition); //Creating an object with all of the data
  return $sqlData;
}

function updateSecureNested($db, $data, $table) {
  foreach ($data as $key => $value) {
    $fieldsString = fieldsToString($value['fields']);
    $duplicate = updateFieldsToString($value['fields']);
    $id = $value['id'];
    $sqlData = array(
      "fields" => $fieldsString,
      "table" => "$table",
      "params" => $value['data'],
      "condition" => "WHERE id = $id",
      "bindings" => $value['bindings'],
      "duplicateData" => $duplicate,
      "id" => $id,
    );
    updateDataNested($db, $sqlData);
  }
}

function updateSecure($db, $sqlData) {
  updateData($db, $sqlData);
}

// READ

function loadSqlData($fields, $table, $condition = "") {
  $sqlData = array("fields" => $fields,
    "table" => $table,
    "condition" => $condition);
  return $sqlData;
}

function loadSqlDataParams($fields, $table, $condition, $param) {
  $sqlData = array("fields" => $fields,
    "table" => $table,
    "condition" => $condition,
    "params" => $param);
  return $sqlData;
}

function loadSecure($db, $sqlData) {
  $stmt = readData($db, $sqlData);
  $return = array();
  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
//    foreach ($row as $key => $value) {
    //      $row[$key] = convert_smart_quotes($value);
    //    }
    array_push($return, $row);
  }
  return $return;
}

// Delete

function deleteSqlData($table, $condition, $param) {
  $sqlData = array('table' => $table,
    "condition" => $condition,
    "params" => $param);
  return $sqlData;
}

function loadSecureTableColumns($db, $table) {
  $stmt = getColumnDetails($db, $table);
  $return = array();
  while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
    array_push($return, $row['Field']);
    // print_r($row);
  }
  return $return;
}
