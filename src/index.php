<?php
require 'vendor/autoload.php';
header("Access-Control-Allow-Origin: " . $_SERVER['HTTP_ORIGIN']);
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: X-Requested-With, Content-Type, Accept, Origin, Authorization, AccessToken");
header("Access-Control-Allow-Methods: POST, PUT, GET, OPTIONS, DELETE");
session_start();
date_default_timezone_set('UTC');

$dbConfig = array(
  'host' => 'db-todo-app',
  'user' => 'root',
  'pass' => 'PBEYRJ2!&*dPy6Qn',
  'dbname' => 'todo-app',
);

//Config
$config = ['settings' => [
  'addContentLengthHeader' => false,
  'debug' => true,
  'displayErrorDetails' => true,
  'db' => $dbConfig,
]];

//Starting up new Slim App
$app = new \Slim\App($config);

//Container for DIC
$container = $app->getContainer();

$container['db'] = function ($c) {
  $db = $c['settings']['db'];
  $pdo = new PDO('mysql:host=' . $db['host'] . ';dbname=' . $db['dbname'],
    $db['user'], $db['pass']);
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
  $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

  return $pdo;
};

include_once "imports/defaultIncludes.php";
include_once "imports/functionIncludes.php";
include_once "imports/moduleIncludes.php";

$GLOBALS['db'] = $container['db'];

$app->run();