<?php

use \Psr\Http\Message\ResponseInterface as Response;
use \Psr\Http\Message\ServerRequestInterface as Request;

// A GET Endpoint is Generally used to Read data
$app->get('/test', function (Request $request, Response $response) {

  // TODO Database Stuff follow rules below (Feel free to ask questions)

  // Reading Data From Database
  // loadSecure($this->db, loadSqlData('fields', 'table', 'Where clause'));

  return $response->withJson([
    "API_SETUP" => "Working",
    "Message" => "This is a message from the API",
  ]);
});

// A POST Endpoint is Generally used to Create data
$app->post('/test', function (Request $request, Response $response) {
  // The Below Body variable is used to read data that has been sent from the Frontend via a POST request
  // $body = json_decode(file_get_contents("php://input"), true); //Getting Body Data

  // TODO Database Stuff follow rules below (Feel free to ask questions)

  // Creating Data
  // createSecure($this->db, createSqlData([
  //   'FieldName1' => "Value1",
  //   'FieldName2' => "Value2",
  // ], 'table'));

  return $response->withJson([
    "API_SETUP" => "Working",
    "Message" => "This is a message from the API",
  ]);
});

// A PUT Endpoint is Generally used to Update data
$app->put('/test', function (Request $request, Response $response) {
  // The Below Body variable is used to read data that has been sent from the Frontend via a PUT request
  // $body = json_decode(file_get_contents("php://input"), true); //Getting Body Data

  // TODO Database Stuff follow rules below (Feel free to ask questions)

  // Updating Data
  // updateSecure($this->db, updateSqlData([
  //   'FieldName1' => "Value1",
  //   'FieldName2' => "Value2",
  // ], 'table', $id));

  return $response->withJson([
    "API_SETUP" => "Working",
    "Message" => "This is a message from the API",
  ]);
});